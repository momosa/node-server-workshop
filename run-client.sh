#!/bin/sh -e

echoerr() { echo "$(basename "$0"): [ERROR] $@" 1>&2; }
echoexit() { echoerr "$@"; exit 1; }

PROJECT_DIR="$(dirname "$0" | xargs realpath)"
SERVER_FILE="${PROJECT_DIR}/client/server.js"

validate() {
    which node 1>&2 > /dev/null || echoexit "Please install node"
    [ -f "${SERVER_FILE}" ] || echoexit "Can't find server file ${SERVER_FILE}"
}

run_server() {
    node "${SERVER_FILE}"
}

validate
run_server
