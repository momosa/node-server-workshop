const express = require(`express`)
const cors = require(`cors`)
const swaggerUI = require(`swagger-ui-express`)
const YAML = require(`yamljs`)

const petsRouter = require(`./pets.js`).petRouter
const logRequests = require(`./log-requests.js`).logRequests

const config = require(`../../config.json`)


const swaggerDocument = YAML.load(`${__dirname}/swagger.yaml`)

const app = express()

// Enable all CORS requests (cross-origin requests)
app.use(cors())


// Process request body as json,
// making the object available as req.body
app.use(express.json())

app.use(`*`, logRequests)

app.use(`/pets`, petsRouter);
app.use(`/api-docs`, swaggerUI.serve, swaggerUI.setup(swaggerDocument))
app.use(`/images`, express.static(`${__dirname}/../images`))

app.listen(config.SERVER_PORT, function() {
  console.log(`Running server at ${config.SERVER_HOST}:${config.SERVER_PORT}/`)
})
