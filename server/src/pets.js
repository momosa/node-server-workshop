const express = require(`express`)

const petsUtil = require(`./pets-util.js`)


let petsData = require(`../data/pets.json`)

const petRouter = express.Router();


petRouter.get(`/list/`, function (req, res) {
  res.status(200).json(petsData)
})

petRouter.get(`/find/`, function (req, res) {
  const pets = petsData.filter(pet => petsUtil.isMatch(pet, req.query))
  res.status(200).json(pets)
})

petRouter.get(`/:name/`, function (req, res) {
  const pet = petsData.find(pet => pet.name == req.params.name) || {}
  res.status(200).json(pet)
})

petRouter.post(`/add/`, function (req, res) {
  petsData.push(req.body)
  res.status(200).json(req.body)
})

petRouter.put(`/update/`, function (req, res) {
  const updated = []
  for (const pet of petsData) {
    if (petsUtil.isMatch(pet, req.query)) {
      petsUtil.update(pet, req.body)
      updated.push(pet)
    }
  }
  res.status(200).json(updated)
})

petRouter.delete(`/remove/`, function (req, res) {
  const removed = petsData.filter(pet => petsUtil.isMatch(pet, req.query))
  petsData = petsData.filter(pet => !petsUtil.isMatch(pet, req.query))
  res.status(200).json(removed)
})

exports.petRouter = petRouter
