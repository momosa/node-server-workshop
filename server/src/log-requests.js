function _logReq (req) {
  logItems = [
    req.method,
    req.originalUrl,
  ]
  if (Object.keys(req.body).length > 0) {
    logItems.push(JSON.stringify(req.body))
  }
  console.log(logItems.join(` `))
}

exports.logRequests = (req, res, next) => {
  _logReq(req)
  next()
}
