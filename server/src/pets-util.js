/*  pets-utils.js
 *
 *  Some helpful tools used in handling pet endpoints.
 */


exports.isMatch = (pet, query) => {
  const isMatch = Object.keys(query).every(
    k => pet[k] !== undefined && pet[k] === query[k]
  )
  return isMatch
}

exports.update = (pet, newData) => {
  for (const key of Object.keys(newData)) {
    pet[key] = newData[key]
  }
}
