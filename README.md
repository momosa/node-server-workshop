# Node Server Workshop

![Node with HTTP icon][icon]

Run a little node API server to experiment with.
Learn how to interact using APIs.
This learning tool comes with:

1. Swagger API documentation
1. CRUD (Create, Read, Update, Delete) endpoints
1. A little client server letting you
   talk to the API using Javascript.

The project is intended to be as simple and easy to understand as possible.
So there's not much in the way of error-handling and
other things that would be important for a production environment.

- - -

## Contents

1. [Setup](#setup)

1. [Usage](#usage)

    2.1. [Running the API server](#running_server)

    2.2. [Running the client server](#running_client)

1. [API Documentation](#api_docs)

1. [Calling the server from the CLI](#calling_server_cli)

    4.1. [Installing curl_and_jq](#install_curl_jq)

    4.2. [Calling the server with curl](#calling_with_curl)

    4.3. [CLI exercises](#cli_exercises)

1. [Calling the server from javascript](#calling_server_javascript)

    5.1. [Client file locations](#client_files)

    5.2. [Client exercises](#client_exercises)

1. [Resources](#resources)

1. [Image credits](#image_credits)

- - -

<a name="setup"></a>

## 1\. Setup

**1.** Install npm.

```shell
which npm 2>&1 > /dev/null \
    && echo "npm already installed" \
    || echo "install npm"
```

To install npm, see [npm's docs][install_npm].

**2.** Run `setup.sh` from the project dir:

```shell
./setup.sh
```

<a name="usage"></a>

## 2\. Usage

By default, the API server and client will be accessible at:

- **API server**:
  http://localhost:8080/
  See the [API docs][#api_docs] to communicate with it.
- **Client**:
  http://localhost:8081/
  Open that url in your browser once
  the API server and client are both running.

You can adjust the hosts and ports in `config.json`

<a name="running_server"></a>

### 2.1\. Running the API server

From the project dir:

```shell
./run-server.sh
```

<a name="running_client"></a>

### 2.2\. Running the client server

From the project dir:

```shell
./run-client.sh
```

- - -


<a name="api_docs"></a>

## 3\. API Documentation

Documentation makes APIs much easier to use,
so we've included some Swagger documentation.
To view it:

1. Run the server. See [Running the server](#running_Server)
1. Visit [http://localhost:8080/api-docs/][local-api_docs]

<a name="calling_server_cli"></a>

### 4\. Calling the server from the CLI

We'll cover calling the server from within JavaScript later.
This section covers calling the server using `curl`.
If you want to call the server using a GUI,
the [API documentation page](#api_docs) contains examples
and provides an interface with calling the server.

<a name="install_curl_jq"></a>

### 4.1\. Installing curl and jq

`curl` is a powerful command-line program that can call APIs.
`jq` provides a convenient way to format json output.

Check if `curl` is already installed

```shell
which curl 2>&1 > /dev/null \
    && echo "curl already installed" \
    || echo "install curl"
```

If curl isn't installed, install it.

```shell
# Ubuntu
sudo apt install curl
```

Use `yum` for RedHat, `brew` for OSX, etc.

Now, do the same for `jq`.

```shell
# Check to see if jq is already installed
which jq 2>&1 > /dev/null \
    && echo "jq already installed" \
    || echo "install jq"

# Install jq (Ubuntu)
sudo apt install jq
```

<a name="calling_with_curl"></a>

#### 4.2\. Calling the server with curl

The [API documentation page](#api_docs)
provides a list of endpoints and
`curl` commands for any endpoint,
which you can use with this section.
Some sample commands are given below, as well.

Here's a simple `GET` request, just `curl` and the URL:

```shell
curl 'http://localhost:8080/pets/list/'
```

For consistency, we'll always specify the request type.
While we're at it, let's go ahead and format our output
by piping it into `jq`:

```shell
curl \
    --request GET \
    'http://localhost:8080/pets/list/' | jq
```

Adding a pet requires sending data in a post request:

```shell
curl \
    --request POST \
    --header 'Content-Type: application/json' \
    --data '{"name": "Nominator", "type": "chameleon", "disposition": "ornery", "image": "http://localhost:8080/images/nominator.jpg"}' \
    'http://localhost:8080/pets/add/' | jq
```

Endpoints for removing, updating, and finding pets all use query parameters.
This example is for removing a pet:

```shell
curl \
    --request DELETE \
    'http://localhost:8080/pets/remove/?name=Pkill-9' | jq
```

The `/update/` endpoint is as complex as it gets.
The endpoint finds pets using query params,
then updates those pets using data provided in the request body.

```shell
curl \
    --request PUT \
    --header 'Content-Type: application/json' \
    --data '{"disposition": "jolly"}' \
    'http://localhost:8080/pets/update/?name=Pkill-9' | jq
```

<a name="cli_exercises"></a>

### 4.3\. CLI exercises

Using the examples above and
the API documentation [API documentation](#api_docs)
to help you out,
use curl to do the following:

1. **Oh, CRUD!**
   Get a list of endpoints from
   the API documentation [API documentation](#api_docs).
   Categorize each endpoint as one of the following:
   - **C**reate
   - **R**ead
   - **U**pdate
   - **D**elete

1. **List all the pets**.

1. **Get a pet by name**.
   You should have all of the names from the previous exercise.
   What happens if you try to get a pet that isn't there?

1. **Add a pet**.
   Add your favorite pet or use the data below.
   Note that the pet list will reset once you restart the server.
   - _Name_: Nominator
   - _Type_: Chameleon
   - _Disposition_: Ornery

1. **Remove a pet**.
   Don't worry: pets will come back once you restart the server.

1. **Make Pkill-9 jolly**.
   Update Pkill-9's disposition.

1. **Make all dogs so good!!!**
   Change the disposition of all dogs to "good" with one API call.

1. **Oh, no! Undo!**
   Remove a pet from the list,
   then use the response to add the pet back into the list.

1. **Safety first!**
   Explain how to use `/find/` with `/update/` and `/remove/`
   to ensure you don't mess up the database
   when updating or removing pet info.

- - -

<a name="calling_Server_javascript"></a>

## 5\. Calling the server from javascript

<a name="client_files"></a>

### 5.1\. Client file locations

The various client files are found in `client/public/`:

```
client/public/
├── config.json -> ../../config.json
├── index.html
├── index.js
├── pets-api.js
├── pets-list.js
└── styles.css
```

- `config.json`:
  A link to the `config.json` in the project dir.
  This allows the client to know the server's URL even if you change it
  in `config.json`
- `index.html`:
  The web page we're sticking things into.
- `index.js`:
  The main JS script file.
  Helps place dynamic content.
- `pets-api.js`:
  The API wrapper.
  Lets you talk to the API using JS functions,
  isolating some implementation details about
  how exactly to talk to the API.
  E.g., if an API path changes,
  you may only need to adjust this file.
- `pets-list.js`:
  JS dynamically generating HTML for lists of pets.

<a name="client_exercises"></a>

### 5.1\. Client exercises

1. **Pet of the day**.
   Choose a random pet as the pet of the day.
   Do something fun like display their picture
   with a flashing border or
   make happy shrimp fly around them.

1. **Users pets**.
   Add a form to the webpage
   allowing users to add a pet.
   Hint: Use the `add` function in `pets-api.js`.

1. **Find pets**.
   Add a form to the webpage
   allowing users to find pets.
   Hint: Use the `find` function in `pets-api.js`.

1. **Remove a pet**.
   Add a form to the webpage
   allowing users to remove a pet.
   Hint: Add a `remove` function to `pets-api.js`.

1. **Update a pet**.
   Add a form to the webpage
   allowing users to specify a pet by name and
   update any other attribute of that pet.
   Hint: See the "Nominator" example in
   the [CLI exercises](#cli_exercises).
    

- - -

<a name="resources"></a>

## 6\. Resources

- [Express server intro][express_intro]
- [Fetch API][fetch_api_mdn]
- [Mime types][mime_types]

- - -

<a name="image_credits"></a>

## 7\. Image credits

- "Box turtle on road, face on" by Martin LaBar is licensed under CC BY-NC 2.0. To view a copy of this license, visit https://creativecommons.org/licenses/by-nc/2.0/?ref=openverse. 
- "Chameleon in Hand" by friel is licensed under CC BY-NC-SA 2.0. To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/2.0/?ref=openverse. 


[icon]: icon.png "Icon"

[local_api_docs]: http://localhost:8080/api-docs/
[mime_types]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
[express_intro]: https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Introduction
[fetch_api_mdn]: https://developer.mozilla.org/en-US/docs/Web/API/fetch
[install_npm]: https://docs.npmjs.com/cli/v10/configuring-npm/install
