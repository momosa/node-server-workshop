/******************************************************************************* 
 *
 * petsApi
 *
 * Interface with the pets API.
 * See the API docs for more details
 *
 *
 ******************************************************************************/

let _BASE_URL = undefined
async function _getBaseUrl() {
  if (_BASE_URL !== undefined) {
    return _BASE_URL
  } else {
    return fetch(`./config.json`)
      .then(res => res.json())
      .then(config => {
        _BASE_URL = `${config.SERVER_HOST}:${config.SERVER_PORT}`
        return _BASE_URL
      })
    .catch(console.error)
  }
}


/******************************************************************************* 
 * pets.list()
 */
async function list () {
  const BASE_URL = await _getBaseUrl()
  const url = new URL(`${BASE_URL}/pets/list/`)
  const options = {
    method: `GET`,
  }
  return fetch(url, options).then(res => res.json())
}


/*******************************************************************************
 * pets.find({type: `dog`})
 */
async function find (query) {
  const BASE_URL = await _getBaseUrl()
  const url = new URL(`${BASE_URL}/pets/find/`)
  for (const [key, value] of Object.entries(query)) {
    url.searchParams.append(key, value)
  }
  const options = {
    method: `GET`,
  }
  return fetch(url, options).then(res => res.json())
}


/*******************************************************************************
 * pets.add({
 *   name: `Nominator`,
 *   type: `chameleon`,
 *   disposition: "ornery`,
 *   image: `http://localhost:8080/image/nominator.jpg`,
 * })
 */
async function add (pet) {
  const BASE_URL = await _getBaseUrl()
  const url = new URL(`${BASE_URL}/pets/add/`)
  const options = {
    method: `POST`,
    headers: Headers({'content-type': 'application/json'}),
    body: JSON.stringify(pet),
  }
  return fetch(url, options).then(res => res.json())
}

export default {
  list,
  find,
  add,
}
