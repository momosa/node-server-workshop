/*******************************************************************************
 * petsList.createAll()
 *
 */


import petsApi from "./pets-api.js"

function _getPetHtml(pet) {
  return [
    `<div class="pet">`,
      `<img src="${pet.image}" />`,
      `<div>${pet.name} the ${pet.type}</div>`,
      `<div>is ${pet.disposition}</div>`,
    `</div>`,
  ].join(``)
}


async function createAll () {
  petsApi.list()
    .then( petsData => {
      const petsHtmlList = petsData.map(pet => _getPetHtml(pet))
      const petsHtml = petsHtmlList.join(``)
      const elements = document.getElementsByClassName(`pet-list`)
      for (const el of elements) {
        el.innerHTML = petsHtml
      }
    })
    .catch(console.error)
}

export default {
  createAll,
}
