const express = require(`express`)
const config = require(`../config.json`)

const BASE_URL = `${config.CLIENT_HOST}:${config.CLIENT_PORT}`


const app = express()

app.use(`/`, express.static(`${__dirname}/public`))

app.listen(config.CLIENT_PORT, function() {
  console.log(`Running server at ${BASE_URL}/`)
})
