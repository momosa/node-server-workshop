#!/bin/sh -ex

PROJECT_DIR="$(dirname "$0" | xargs realpath)"
START_DIR="$PWD"

echoerr() { echo "$(basename "$0"): [ERROR] $@" 1>&2; }
echoexit() { echoerr "$@"; exit 1; }

validate() {
    which npm 1>&2 > /dev/null \
        || echoexit "Please install npm. See https://docs.npmjs.com/cli/v10/configuring-npm/install"
}

setup_server() {
    cd "${PROJECT_DIR}/server"
    npm install || true
    npm audit fix || true
    cd "$START_DIR"
}

setup_client() {
    cd "${PROJECT_DIR}/client"
    npm install || true
    npm audit fix || true
    cd "$START_DIR"
}

validate
setup_server
setup_client
